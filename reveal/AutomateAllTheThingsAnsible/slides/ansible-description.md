# Ansible

---

## Allgemein

---

* Automatisierung <!-- .element: class="fragment" data-fragment-index="1" -->
* Konfiguration von Systemen <!-- .element: class="fragment" data-fragment-index="2" -->
* DevOps Werkzeug <!-- .element: class="fragment" data-fragment-index="3" -->

---

## Aufbau

---

* Playbooks <!-- .element: class="fragment" data-fragment-index="1" -->
* Inventories <!-- .element: class="fragment" data-fragment-index="2" -->
* Group-Vars <!-- .element: class="fragment" data-fragment-index="3" -->
* Host-Vars <!-- .element: class="fragment" data-fragment-index="4" -->

---

## Playbooks

---

* das Herzstück <!-- .element: class="fragment" data-fragment-index="1" -->
* Einbindung aller Rollen die auf ein Ziel / mehrere Ziele angewendet werden sollen <!-- .element: class="fragment" data-fragment-index="2" -->
* Kann auch Tasks enthalten <!-- .element: class="fragment" data-fragment-index="3" -->
* Wird sequentiell abgearbeitet <!-- .element: class="fragment" data-fragment-index="4" -->

---

## Inventories

---

* Verwaltung von Zielen für die Playbooks <!-- .element: class="fragment" data-fragment-index="1" -->
* Zusammenfassung von einzelnen Maschinen zu Gruppen <!-- .element: class="fragment" data-fragment-index="2" -->

---

## Group-Vars

---

* Variablen die für Gruppen von Hosts bestimmt sind <!-- .element: class="fragment" data-fragment-index="1" -->
    * appservers <!-- .element: class="fragment" data-fragment-index="2" -->
* müssen nicht aktiv eingebunden werden <!-- .element: class="fragment" data-fragment-index="3" -->
* aber an der richtigen Stelle liegen <!-- .element: class="fragment" data-fragment-index="4" -->

---

## Host-Vars

---

* Variablen die für bestimmte Hosts bestimmt sind <!-- .element: class="fragment" data-fragment-index="1" -->
    * hostname <!-- .element: class="fragment" data-fragment-index="2" -->
* müssen nicht aktiv eingebunden werden <!-- .element: class="fragment" data-fragment-index="3" -->
* aber an der richtigen Stelle liegen <!-- .element: class="fragment" data-fragment-index="4" -->

---

## Var-Files

---

* müssen an keinem fixen Ort liegen (System ist aber empfehlenswert) <!-- .element: class="fragment" data-fragment-index="1" -->
* müssen im Playbook eingebunden werden <!-- .element: class="fragment" data-fragment-index="2" -->
* ermöglichen die Konfiguration von Rollen <!-- .element: class="fragment" data-fragment-index="3" -->

---

## Rollen

---

* Ansible Galaxy <!-- .element: class="fragment" data-fragment-index="1" -->
    * Vielzahl an Rollen die verwendet werden können <!-- .element: class="fragment" data-fragment-index="2" -->
    * Verwaltung in requirements.yml <!-- .element: class="fragment" data-fragment-index="3" -->
    * Installation mit ansible-galaxy -r requirements.yml <!-- .element: class="fragment" data-fragment-index="4" -->

---

## SecretManagement

---

* Ansible Vault zur Verwaltung von Geheimnissen <!-- .element: class="fragment" data-fragment-index="1" -->
* Daten werden in einer Datei verschlüsselt (Diese kann im Repo liegen)<!-- .element: class="fragment" data-fragment-index="2" -->
* kann mit einer Passwort-Datei verwendet werden (sollte niemals Teil des Repositories sein) <!-- .element: class="fragment" data-fragment-index="3" -->
* wird von Ansible zur Laufzeit enschlüsselt <!-- .element: class="fragment" data-fragment-index="4" -->
* enthält yml-formatierte Werte (analog zu Var-files)<!-- .element: class="fragment" data-fragment-index="5" -->

---

## Einsatzgebiete

---

* kann auf dem Zielsystem lokal laufen <!-- .element: class="fragment" data-fragment-index="1" -->
* kann sich per SSH auf ein Zielsystem verbinden <!-- .element: class="fragment" data-fragment-index="2" -->
* kann (je nach Qulität der Rollen) mehrfach ausgeführt werden <!-- .element: class="fragment" data-fragment-index="3" -->
* kann theoretisch auch Infrastruktur verwalten (AWS) <!-- .element: class="fragment" data-fragment-index="4" -->
    * nicht empfehlenswert - lieber Terraform benutzen <!-- .element: class="fragment" data-fragment-index="5" -->

---

## Ansible bei uns

---

## Hauptaufgaben

---

* Einheitliche Grundkonfiguration aller unserer AWS-Server <!-- .element: class="fragment" data-fragment-index="1" -->
* BaseImages werden mit Ansible provisioniert <!-- .element: class="fragment" data-fragment-index="2" -->
* Hardening der Images (Linux/SSH/Nginx/Apache) <!-- .element: class="fragment" data-fragment-index="3" -->

---

## Während des Packer-Runs

---

* Wird beim Build-Prozess ausgeführt <!-- .element: class="fragment" data-fragment-index="1" -->
    * Keine manuelle Ausführung <!-- .element: class="fragment" data-fragment-index="2" -->
* Packer erstellt/verwaltet das Inventory <!-- .element: class="fragment" data-fragment-index="3" -->
* wird nie mehr als einmal ausgeführt <!-- .element: class="fragment" data-fragment-index="4" -->
* Secrets werden immer mit ansible-vault verschlüsselt <!-- .element: class="fragment" data-fragment-index="5" -->

---

## Während der Boot-Zeit

---

* Ansible läuft direkt auf dem Server <!-- .element: class="fragment" data-fragment-index="1" -->
* wird vom cloudinit-skript ausgeführt <!-- .element: class="fragment" data-fragment-index="2" -->
* setzt Zugangsdaten <!-- .element: class="fragment" data-fragment-index="3" -->
* konfiguriert vHost <!-- .element: class="fragment" data-fragment-index="4" -->

---

## Vielen Dank

---

## Seit auch nächstes mal dabei, wenn es wieder heisst

---

# Automate All The Things <!-- .element: class="fragment" data-fragment-index="1" -->
## Terraform - Edition <!-- .element: class="fragment" data-fragment-index="2" -->
